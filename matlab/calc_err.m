str = fileread('result');
split = strsplit(str, '\n');
elcount = length(split)-1;
step = 0.02;
t = [0:step:step*(elcount-1)];
Y = zeros([1, elcount]);
ref = zeros([1, elcount]);

for i=1:elcount
    s = strsplit(string(split(i)), ' ');
    Y(i) = str2double(s(1));
    ref(i) = str2double(s(2));
end

df = ref' - Y';
df = df.^2;
E = sum(df);
mean_E = sqrt(E/elcount)

