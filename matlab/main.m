close all
clc
clear all
str = fileread('23_01_16');
split = strsplit(str, '\n');
elcount = length(split)-1;
step = 0.02;
t = [0:step:step*(elcount-1)];
Y = zeros([1, elcount]);
control = zeros([1, elcount]);

for i=1:elcount
    s = strsplit(string(split(i)), ' ');
    Y(i) = str2double(s(1));
    control(i) = str2double(s(2));
end

Y_filt = lowpass(Y, 0.02);

Y_spd = diff(Y_filt)/step;
Y_spd = lowpass(Y_spd, 0.02);

Y_acc = diff(Y_spd)/step;
% Y_acc = lowpass(Y_acc, step);



S = [-Y_filt(50:end-50);-Y_spd(49:end-50);control(50:end-50)]';
R = Y_acc(48:end-50)';
params = pinv(S)*R;
sys = tf([params(3)],[1 params(2) params(1)])
res = lsim(sys, control, t, 0);

hold on
% plot(t,Y_filtered,t,level);
% plot(t(2:end), lowpass(Y_spd, 0.01));
% plot(t(2:end), lowpass(Y_spd, 0.05));
plot(t, res);
plot(t, Y_filt);




