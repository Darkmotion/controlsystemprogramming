P = C.Kp;
I = C.Ki;
D = C.Kd;
N = C.Tf;

pid_P = tf(P,1);
pid_I = tf(I, [1 0]);
pid_D = tf([D 0], [N 1]);
PID = pid_P + pid_I+pid_D;
pid_ss = ss(PID);
pid_dscr = c2d(pid_ss, 0.02)