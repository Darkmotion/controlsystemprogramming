#include "controller.h"
Controller::Controller(QObject *parent) : QObject(parent)
{
    x1 = 0;
    x2 = 0;

}

Controller::~Controller()
{

}

void Controller::getSignal(float err)
{
    float y;
    y = 13.423060121327518 * x1 + 15.731959997055286 * x2 - 0.0000000000000161772022*err;

    x1 = 0.407919747043066 * x1 + 0.422591475229342*err;
    x2 = 0.013205983600917 * x1 + x2 + 0.004849162589838*err;

    emit genSignal(y);
}

