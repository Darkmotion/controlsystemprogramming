#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QDebug>

class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(QObject *parent = nullptr);
    ~Controller();

private:
    double x1 = 0;
    double x2 = 0;

public slots:
    void getSignal(float err);

signals:
    void genSignal(float output);
};

#endif // CONTROLLER_H
