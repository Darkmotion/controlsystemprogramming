#include "custom_logger.h"

CuLogger::CuLogger(QObject *parent) : QObject(parent)
{
    file = new QFile(QDateTime::currentDateTime().toString("hh_mm_ss"));
    if (!file -> open(QIODevice::WriteOnly)){
        qDebug() << "Failed attempt to open the file";
    };
    textStream = new QTextStream(file);
}

CuLogger::~CuLogger()
{
    file->close();

    delete file;
    delete textStream;
}

void CuLogger::log(float val, float ref)
{
    *textStream << val << " " << ref << "\n";
}
