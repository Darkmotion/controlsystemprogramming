#ifndef CUSTOM_LOGGER_H
#define CUSTOM_LOGGER_H

#include <QObject>
#include <QTextStream>
#include <QIODevice>
#include <QFile>
#include <QDateTime>
#include <QDebug>

class CuLogger : public QObject
{
    Q_OBJECT
public:
    explicit CuLogger(QObject *parent = nullptr);
    ~CuLogger();

private:
    QFile* file;
    QTextStream* textStream;

public slots:
    void log(float val, float ref);

signals:

};

#endif // CUSTOM_LOGGER_H
