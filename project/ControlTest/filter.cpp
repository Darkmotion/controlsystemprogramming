#include "filter.h"

Filter::Filter(QObject *parent) : QObject(parent)
{
    x = new integrator(0);
}

void Filter::getSignal(float input, float lvl)
{
    std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
    float dt = std::chrono::duration_cast<std::chrono::microseconds>(now - previous_tick).count()/1000000.0;
    float x_st = x->getState();
    float y = 6.283*x_st;
    x->update(-25.13*x_st +  4*input, dt);
    previous_tick = now;
    qDebug() << "Filtered " << y ;
    emit genSignal(lvl - y);
}
