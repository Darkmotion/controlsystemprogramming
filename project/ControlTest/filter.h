#ifndef FILTER_H
#define FILTER_H

#include <QObject>
#include <QDebug>
#include <integrator.h>
#include <chrono>

class Filter : public QObject
{
    Q_OBJECT
public:
    explicit Filter(QObject *parent = nullptr);

public slots:
    void getSignal(float input, float lvl);

private:
    integrator *x;
    std::chrono::high_resolution_clock::time_point previous_tick;

signals:
    void genSignal(float output);

};

#endif // FILTER_H
