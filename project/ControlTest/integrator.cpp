#include "integrator.h"

integrator::integrator(float initial)
{
    state = initial;
    previous_value = initial;
}

float integrator::update(float current_value, float step)
{
    state = state + (current_value+previous_value)*step/2.0;
    previous_value = current_value;
    return state;
}

float integrator::getState() const
{
    return state;
}
