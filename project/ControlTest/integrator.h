#ifndef INTEGRATOR_H
#define INTEGRATOR_H


class integrator
{
public:
    integrator(float initial);
    float update(float current_value, float step);
    float getState() const;

private:
    float state;
    float previous_value;
};
#endif // INTEGRATOR_H
