#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "./lib/controllerbackend.h"
#include "controller.h"
#include "parser.h"
#include "filter.h"
#include "modulator.h"
#include "custom_logger.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    Parser parser;
    Filter filter;
    Controller control;
    Modulator modulator;
    CuLogger lg;

    QApplication app(argc, argv);

    ControllerBackend controllerBackend(50, "Zsdnw82iuw");

    QQmlApplicationEngine engine;

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    engine.rootContext()->setContextProperty("plant", &controllerBackend);

    QObject::connect(&controllerBackend, &ControllerBackend::outputBytesChanged,
                     &parser, &Parser::getMsg);

    QObject::connect(&parser, &Parser::tick,
                     &modulator, &Modulator::tick);

    QObject::connect(&parser, &Parser::genSignal,
                     &lg, &CuLogger::log);


    QObject::connect(&parser, &Parser::genSignal,
                     &filter, &Filter::getSignal);

    QObject::connect(&modulator, &Modulator::setLvl,
                     &controllerBackend, &ControllerBackend::setReferenceSignal);
    QObject::connect(&filter, &Filter::genSignal,
                     &control, &Controller::getSignal);

    QObject::connect(&control, &Controller::genSignal,
                     &controllerBackend, &ControllerBackend::setInput);

//    QObject::connect(&controllerBackend, &ControllerBackend::outputBytesChanged,
//                     &testController, &TestController::computeBytes);

//    QObject::connect(&testController, &TestController::computed,
//                     &controllerBackend, &ControllerBackend::setInput);

    engine.load(url);


    return app.exec();
}
