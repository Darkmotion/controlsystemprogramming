#include "modulator.h"

Modulator::Modulator(QObject *parent) : QObject(parent)
{

}

void Modulator::tick()
{
    i++;
    float val, time;
    time = 0.02*i;
    val = 70-10*std::exp(-0.1*time) * ((60*time - std::pow(time-2,2 ))/100)*std::sin(time/2);
    emit setLvl(val);
}
