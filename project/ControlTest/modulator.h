#ifndef MODULATOR_H
#define MODULATOR_H

#include <QObject>
#include <math.h>

class Modulator : public QObject
{
    Q_OBJECT
public:
    explicit Modulator(QObject *parent = nullptr);

public slots:
    void tick();

private:
    int i=0;


signals:
    void setLvl(float input);
};

#endif // MODULATOR_H
