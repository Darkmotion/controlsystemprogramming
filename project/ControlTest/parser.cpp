#include "parser.h"

Parser::Parser(QObject *parent) : QObject(parent)
{

}

void Parser::getMsg(QByteArray input)
{
    quint8 sum = input[10];
    quint8 real_sum = 0;

    float y_out;
    float level;


    for (int i=0; i<10; i++){
        real_sum += input[i];
    }
    real_sum = 0xff - real_sum;
    qDebug() << input.toHex(' ');
    if (real_sum == sum){
        memcpy(&y_out,input.data()+2, 4);
        memcpy(&level, input.data()+6, 4);
        emit genSignal(y_out, level);
    }else{
     qDebug() << "Incorrect check sum";
    }
    emit tick();
}
