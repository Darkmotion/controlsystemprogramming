#ifndef PARSER_H
#define PARSER_H

#include <QObject>
#include <QDebug>

class Parser : public QObject
{
    Q_OBJECT
public:
    explicit Parser(QObject *parent = nullptr);

public slots:
    void getMsg(QByteArray input);

signals:
    void genSignal(float y, float level);
    void tick();
};

#endif // PARSER_H
